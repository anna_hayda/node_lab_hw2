const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {createNote, getNote, getById, putById, patchById, deleteById} =
    require('../controllers/notesControllers');
const {authMiddleware} = require('./middleware/authMiddleware');

router.post('/notes', authMiddleware, asyncWrapper(createNote));
router.get('/notes', authMiddleware, asyncWrapper(getNote));
router.get('/notes/:id', authMiddleware, asyncWrapper(getById));
router.put('/notes/:id', authMiddleware, asyncWrapper(putById));
router.patch('/notes/:id', authMiddleware, asyncWrapper(patchById));
router.delete('/notes/:id', authMiddleware, asyncWrapper(deleteById));

module.exports = router;
