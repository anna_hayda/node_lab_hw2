const express = require('express');
const bcrypt = require('bcrypt');
const router = new express.Router();
const {User} = require('../models/userModel');
const {authMiddleware} = require('./middleware/authMiddleware');

//  api/users/me
router.get('/me', authMiddleware, async (req, res) => {
  const getUser = await User.findById(req.user._id);
  if (!getUser) {
    res.status(400).json({message: 'Please log in'});
  }
  return res.status(200).json({
    user: {
      id: getUser._id,
      username: getUser.username,
      createdDate: getUser.createdDate,
    },
  });
});

router.delete('/me', authMiddleware, async (req, res) => {
  await User.findByIdAndRemove(req.user._id, req.body, (err) => {
    if (!err) {
      res.status(200).json({message: 'Success'});
    } else {
      res.status(400).json({message: 'Something went wrong'});
    }
  });
});


router.patch('/me', authMiddleware, async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id, 'password').exec();

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'wrong password'});
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();

  res.status(200).json({message: 'Success'});
});


module.exports = router;
