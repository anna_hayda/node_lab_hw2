const Note = require('../models/noteModel');

module.exports.createNote = async (req, res) => {
  const text = req.body.text;
  const userId = req.user._id;
  const note = new Note({
    userId: userId,
    completed: false,
    text: text,
  });
  if (!userId) {
    res.status(400).json({message: 'You should log in.'});
  }
  await note.save();
  res.status(200).json({message: 'Success!'});
};

module.exports.getNote = async (req, res) => {
  const userId = req.user._id;
  const {limit = 6, offset = 0} = req.query;
  const userNotes = await Note.find({userId}, {__v: 0}, {
    limit: parseInt(limit),
    skip: parseInt(offset),
    sort: {
      createdDate: -1,
    },
  });

  res.status(200).json({
    notes: userNotes,
  });
};

module.exports.getById = async (req, res) => {
  const note = await Note.findOne({_id: req.params.id}, {__v: 0});

  if (!note) {
    return res.status(400).json({message: 'Note not found.'});
  }
  res.status(200).send({note});
};

module.exports.putById = async (req, res) => {
  const {text} = req.body;
  const note = await Note.findOne({_id: req.params.id});
  if (!note) {
    res.status(400).json({message: 'Note not found.'});
  }
  note.text = text;
  await note.save();
  res.status(200).json({message: 'Updated!'});
};

module.exports.patchById = async (req, res) => {
  const _id = req.params.id;
  const note = await Note.findOne({_id, userId: req.user._id});
  if (!note) {
    res.status(400).json({message: 'Note not found.'});
  }

  await Note.updateOne({_id: req.params.id}, {completed: !this.completed});
  res.status(200).json({message: 'Success!'});
};

module.exports.deleteById = async (req, res) => {
  const _id = req.params.id;
  if (!Note.findOne({_id, userId: req.user._id})) {
    res.status(400).json({message: 'Note not found.'});
  }

  await Note.findByIdAndDelete({_id: req.params.id});
  res.status(200).json({message: 'Deleted!'});
};
