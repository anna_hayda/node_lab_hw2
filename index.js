const mongoose = require('mongoose');
const express = require('express');
const morgan = require('morgan');
const app = express();

const authRouter = require('./routers/authRouter');
const getProfileInfo = require('./routers/getProfileInfo');
const notesRouter = require('./routers/notesRouter');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', getProfileInfo);
app.use('/api', notesRouter);

/** Class representing a statusCode. */
class UnauthorizedError extends Error {
  /**
     * @param {string} message - The message value.
     */
  constructor(message = 'Unauthorized user') {
    super(message);
    statusCode = 400;
  }
}

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect(
      'mongodb+srv://testuser:123QWEasd@cluster0.k7uxm.mongodb.net/node_lab_hw2?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );
  const PORT = process.env.PORT || 8080;
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
};

start();
