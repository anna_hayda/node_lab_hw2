const {Schema, model} = require('mongoose');
const schema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    require: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = model('Note', schema);
